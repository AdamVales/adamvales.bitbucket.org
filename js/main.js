function activateLightbox(element){
    $(".light-box-overlay").toggleClass("active");
    $(element).toggleClass('active');
}

$(document).ready(function(){

    $( function()
    {
        var targets = $( '[rel~=tooltip]' ),
            target  = false,
            tooltip = false,
            title   = false;

        targets.bind( 'mouseenter', function()
        {
            target  = $( this );
            tip     = target.attr( 'title' );
            tooltip = $( '<div id="tooltip"></div>' );

            if( !tip || tip == '' )
                return false;

            target.removeAttr( 'title' );
            tooltip.css( 'opacity', 0 )
                .html( tip )
                .appendTo( 'body' );

            var init_tooltip = function()
            {
                if( $( window ).width() < tooltip.outerWidth() * 1.5 )
                    tooltip.css( 'max-width', $( window ).width() / 2 );
                else
                    tooltip.css( 'max-width', 340 );

                var pos_left = target.offset().left + ( target.outerWidth() / 2 ) - ( tooltip.outerWidth() / 2 ),
                    pos_top  = target.offset().top - tooltip.outerHeight() - 20;

                if( pos_left < 0 )
                {
                    pos_left = target.offset().left + target.outerWidth() / 2 - 20;
                    tooltip.addClass( 'left' );
                }
                else
                    tooltip.removeClass( 'left' );

                if( pos_left + tooltip.outerWidth() > $( window ).width() )
                {
                    pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
                    tooltip.addClass( 'right' );
                }
                else
                    tooltip.removeClass( 'right' );

                if( pos_top < 0 )
                {
                    var pos_top  = target.offset().top + target.outerHeight();
                    tooltip.addClass( 'top' );
                }
                else
                    tooltip.removeClass( 'top' );

                tooltip.css( { left: pos_left, top: pos_top } )
                    .animate( { top: '+=10', opacity: 1 }, 50 );
            };

            init_tooltip();
            $( window ).resize( init_tooltip );

            var remove_tooltip = function()
            {
                tooltip.animate( { top: '-=10', opacity: 0 }, 50, function()
                {
                    $( this ).remove();
                });

                target.attr( 'title', tip );
            };

            target.bind( 'mouseleave', remove_tooltip );
            tooltip.bind( 'click', remove_tooltip );
        });
    });

    var topStory = new Swiper ('.swiper-container.top-story', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,

        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    })

    var actionProducts = new Swiper ('.swiper-container.action', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,
        slidesPerView: 3,
        spaceBetween: 30,
        // Navigation arrows
        breakpoints: {
            // when window width is <= 320px
            992: {
                slidesPerView: 2,
                spaceBetweenSlides: 10
            },
            // when window width is <= 480px
            600: {
                slidesPerView: 1,
                spaceBetweenSlides: 20
            },
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    var actionProducts = new Swiper ('.products.five-products .swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,
        slidesPerView: 5,
        spaceBetween: 5,
        // Navigation arrows
        breakpoints: {
            // when window width is <= 320px
            992: {
                slidesPerView: 2,
                spaceBetweenSlides: 10
            },
            // when window width is <= 480px
            600: {
                slidesPerView: 1,
                spaceBetweenSlides: 20
            },
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    var actionProducts = new Swiper ('.products.four-products .swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,
        slidesPerView: 4,
        spaceBetween: 5,
        // Navigation arrows
        breakpoints: {
            // when window width is <= 320px
            992: {
                slidesPerView: 2,
                spaceBetweenSlides: 10
            },
            // when window width is <= 480px
            600: {
                slidesPerView: 1,
                spaceBetweenSlides: 20
            },
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

        // simple tabs
        $('ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    });

        $('.filters .heading').click(function(){
            $(this).parent().toggleClass('mobile-active');
        });
    $('.side-nav .tabs *').click(function(){
        $(this).parent().parent().toggleClass('mobile-active');
    });
});

var slider = document.getElementById('slider');

noUiSlider.create(slider, {
    start: [20, 80],
    connect: true,
    range: {
        'min': 0,
        'max': 100
    }
});

var snapValues = [
    document.getElementById('slider-value-lower'),
    document.getElementById('slider-value-upper')
];

slider.noUiSlider.on('update', function( values, handle ) {
    snapValues[handle].innerHTML = values[handle];
});


